using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace GeoSharp
{
	public class Program 
	{
		public static void Main(string[] args) 
		{
			Console.WriteLine("Running GeoSharp...");
			
			if (args.Length == 1) 
			{
				Console.WriteLine("Reading '" + args[0] + "'");
				string[,] locations = Program.LoadCsv(args[0]);
				Console.WriteLine("Loading " + locations.GetUpperBound(0) + " locations");
				
				GeoTree tree = new GeoSharp.GeoTree();
				int NAME_IDX = 0;
				int LAT_IDX = 1;
				int LNG_IDX = 2;
				
				for (int i = 0; i < locations.GetUpperBound(0); i++) {
					string name = locations[i, NAME_IDX];
					string latStr = locations[i, LAT_IDX];
					string lngStr = locations[i, LNG_IDX];
					decimal lat, lng;
					Decimal.TryParse(latStr, out lat);
					Decimal.TryParse(lngStr, out lng);
					
					tree.Insert(lat, lng, name);
				}
				
				Random rnd = new Random();
				int l = rnd.Next( locations.GetUpperBound(0) );
				string searchName = locations[l, NAME_IDX];
				string searchLatStr = locations[l, LAT_IDX];
				string searchLngStr = locations[l, LNG_IDX];
				Console.WriteLine("Searching random location:");
				Console.WriteLine(" \"" + searchName + "\" at ("+searchLatStr+","+searchLngStr+")");
				
				decimal searchLat, searchLng;
				Decimal.TryParse(searchLatStr, out searchLat);
				Decimal.TryParse(searchLngStr, out searchLng);				
				int distance = 10000;
				Location[] results = tree.GetLocations(searchLat, searchLng, distance, 100);
				
				Console.WriteLine(" Found " + results.Length + " nearby locations");
			}
		} 
		
		private static string[,] LoadCsv(string filename)
		{
		    string text = System.IO.File.ReadAllText(filename);
			bool hasHeaders = true;
		
		    text = text.Replace('\n', '\r');
		    string[] lines = text.Split(new char[] { '\r' }, StringSplitOptions.RemoveEmptyEntries);
			if (hasHeaders) {
				var lineList = new System.Collections.Generic.List<string>(lines);
				lineList.RemoveAt(0);
				lines = lineList.ToArray();
			}
			
			Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
			
		    int rows = lines.Length;
			if (rows == 0) {
				return new string[0, 0];
			}
				
		    int cols = CSVParser.Split(lines[0]).Length;
		    string[,] values = new string[rows, cols];
			
		    for (int row = 0; row < rows; row++)
		    {
				string line = lines[row];
        		string[] fields = CSVParser.Split(line);
		
		        for (int col = 0; col < cols; col++)
		        {
					string val = fields[col].TrimStart(' ', '"').TrimEnd('"');
		            values[row, col] = val;
		        }
		    }
		
		    return values;
		}
	}
	
}