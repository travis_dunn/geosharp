using System;
using System.Collections.Generic;

namespace GeoSharp
{
    public enum RedBlack
    {
		Red,
		Black
    };
	
	public class RBNode 
	{
        public decimal Key { get; set; }
        public List<Location> Values { get; set; }
        public RBNode Parent { get; set; }
        public RedBlack Color { get; set; }
        public RBNode Left { get; set; }
        public RBNode Right { get; set; }
		
        public RBNode(RBNode parent, decimal key, Location value)
        {
			Parent = parent;
            Key = key;
            Values = new List<Location>();
            Values.Add(value);
			Left = null;
			Right = null;
			Color = RedBlack.Red;
        }
        
        public RBNode GetGrandparent()
        {
            return Parent != null ? Parent.Parent : null;
        }
        
        public RBNode GetUncle()
        {
            RBNode grandparent = this.GetGrandparent();
            if (grandparent != null) {
                return grandparent.Left == Parent ? grandparent.Right : grandparent.Left;
            } else {
                return null;
            }
        }      
	}
    
	public class RBTree 
    {
        public RBNode Root { get; set; }
     
        public void Insert(decimal key, Location value) 
        {
            RBNode node = null, parent = null, grandparent = null, uncle = null;
            
            if (Root == null) {
                Root = new RBNode(null, key, value);
                node = Root;
            } else {
                parent = Root;
                while (true) {
                    if (parent.Key == key) 
                    {
                        parent.Values.Add(value);
                        return;
                    }
                    if (key < parent.Key) 
                    {
                        if (parent.Left != null) 
                        { 
                            parent = parent.Left; 
                        } 
                        else 
                        { 
                            node = parent.Left = new RBNode(parent, key, value);
                            break; 
                        }
                    } 
                    else 
                    {
                        if (parent.Right != null) 
                        { 
                            parent = parent.Right; 
                        } 
                        else 
                        { 
                            node = parent.Right = new RBNode(parent, key, value);
                            break; 
                        }
                    }
                }
            }
            
            grandparent = node.GetGrandparent();
            uncle = node.GetUncle();
            
            while (true) {
                if (parent == null) 
                { 
                    node.Color = RedBlack.Black; 
                    break; 
                } 
                else if (parent.Color == RedBlack.Black) 
                { 
                    break; 
                }
                else if (uncle != null && uncle.Color == RedBlack.Red) 
                { 
                    parent.Color = uncle.Color = RedBlack.Black;
                    grandparent.Color = RedBlack.Red;
                    node = grandparent; 
                    parent = node.Parent; 
                    grandparent = node.GetGrandparent(); 
                    uncle = node.GetUncle();
                    continue; 
                }
                        
                if (node == parent.Right && parent == grandparent.Left) 
                {
                    grandparent.Left = node; 
                    node.Parent = grandparent;
                    
                    parent.Right = node.Left;
                    if (parent.Right != null) { node.Left.Parent = parent; }
                    node.Left = parent; 
                    parent.Parent = node;
                    node = parent; 
                    parent = node.Parent;
                } 
                else if (node == parent.Left && parent == grandparent.Right) 
                {
                    grandparent.Right = node; 
                    node.Parent = grandparent;
                    
                    parent.Left = node.Right;
                    if (parent.Left != null) { node.Right.Parent = parent; }
                    node.Right = parent; 
                    parent.Parent = node;
                    node = parent; 
                    parent = node.Parent;
                }     
                
                parent.Color = RedBlack.Black;
                grandparent.Color = RedBlack.Red;
                if (node == parent.Left) 
                {
                    grandparent.Left = parent.Right;
                    if (grandparent.Left != null) { parent.Right.Parent = grandparent; }
                    parent.Right = grandparent;
                } 
                else 
                {
                    grandparent.Right = parent.Left;
                    if (grandparent.Right != null) { parent.Left.Parent = grandparent; }
                    parent.Left = grandparent;
                }
                RBNode parentOfGrandparent = grandparent.Parent;
                if (parentOfGrandparent != null) 
                { 
                    if (grandparent == parentOfGrandparent.Left) 
                    { 
                        parentOfGrandparent.Left = parent; 
                    } 
                    else 
                    { 
                        parentOfGrandparent.Right = parent; 
                    } 
                }
                else 
                { 
                    Root = parent; 
                    parent.Color = RedBlack.Black; 
                }
                parent.Parent = parentOfGrandparent; 
                grandparent.Parent = parent;
                break;           
                
            }
        } 
        
        public Location[] Find(decimal start, decimal end) 
        {
            if (Root == null) 
            { 
                return new Location[0]; 
            }
            
            RBNode node = null;   
            List<RBNode> stack = new List<RBNode>();
            stack.Add(Root);
            
            List<Location[]> values = new List<Location[]>();
            
            while (stack.Count > 0) {
                node = stack[stack.Count - 1];
                stack.RemoveAt(stack.Count - 1);
                
                if (node.Key >= start && node.Key <= end) 
                { 
                    values.Add(node.Values.ToArray()); 
                }
                if (node.Right != null && node.Key < end) 
                { 
                    stack.Add(node.Right); 
                }
                if (node.Left != null && node.Key > start) 
                { 
                    stack.Add(node.Left); 
                }
            }
            
            List<Location> locations = new List<Location>();
            
		    for (int i = 0; i < values.Count; i++)
		    {
                Location[] loc = values[i];
                for (int j = 0; j < loc.Length; j++) {
                    locations.Add(loc[j]);
                }
            }
            
            return locations.ToArray(); 
        }  
    }
}
