using System;

namespace GeoSharp
{
	public static class GeoMath 
	{			
		public const double EarthRadiusInMeters = 6371009.0;
		
		public static double AngleToRadians(double angle)
		{
		    return (Math.PI / 180) * angle;
		}		
		
		public static double RadiansToAngle(double radians)
		{
		    return (radians * 180) / Math.PI;
		}	
		
		public static double GetRadius(decimal latitude, long distanceInMeters) {
			double degress = EarthRadiusInMeters * Math.Cos(AngleToRadians( Convert.ToDouble(latitude) ));
			return RadiansToAngle(distanceInMeters / degress);
		}
		
		public static bool LocateInHaversine(decimal centerLat, decimal centerLng, decimal locateLat, decimal locateLng, long distanceInMeters)
		{
			double lat = AngleToRadians((double)(centerLat - locateLat));
			double lng = AngleToRadians((double)(centerLng - locateLng));
		    double sinLat = Math.Sin(lat / 2);
		    double sinLng = Math.Sin(lng / 2);
		    double cosCenterLat = Math.Cos(AngleToRadians((double)centerLat));
		    double cosLocateLat = Math.Cos(AngleToRadians((double)locateLat));
			
      		double a = sinLat * sinLat + cosCenterLat * cosLocateLat * sinLng * sinLng;
			double p = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt( 1 - a));
			
			return EarthRadiusInMeters * p <= distanceInMeters;
		} 
	}
}