using System;

namespace GeoSharp
{
	public static class ZCurve 
	{
		public static long XY2D(long xPos, long yPos)
		{
    		long bit = 1;
			double max = Math.Max(xPos, yPos);
			long result = (long)0.0;
			while (bit <= max) 
			{ 
				bit <<= 1; 
			}
    		bit >>= 1;
	
		    while (bit > 0) {
				result *= (long)2.0;
				if ((xPos & bit) > 0) { result += (long)1.0; }
				result *= (long)2.0;
				if ((yPos & bit) > 0) { result += (long)1.0; }
				bit >>= 1;
			}
	
			return result;
		}
	}
}
