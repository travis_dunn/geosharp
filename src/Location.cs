using System;

namespace GeoSharp
{
	public class Location 
	{
                public decimal Latitude { get; set; }
                public decimal Longitude { get; set; }
                public string Name { get; set; }
        		
                public Location(decimal latitude, decimal longitude, string name)
                {
                    Latitude = latitude;
                    Longitude = longitude;
                    Name = name;
        	}
        }
}