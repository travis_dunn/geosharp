using System;
using System.Collections.Generic;

namespace GeoSharp
{
	public class GeoTree 
	{
		public RBTree Tree { get; set; }
		
		public GeoTree() 
		{
			Tree = new RBTree();
		}
		
		public void Insert(decimal lat, decimal lng, string data)
		{
			decimal latitude = Math.Round((lat + (decimal)90.0) * 100000);
  			decimal longitude = Math.Round((lng + (decimal)180.0) * 100000);
			  
			Location location = new Location(latitude, longitude, data);
			long index = ZCurve.XY2D((long)latitude, (long)longitude);
			Tree.Insert(index, location);
		}
		
		public Location[] GetLocations(Location location, long distanceInMeters, int maxResults) {
			return GetLocations(location.Latitude, location.Longitude, distanceInMeters, maxResults);
			
		}
		
		public Location[] GetLocations(decimal lat, decimal lng, long distanceInMeters, int maxResults) {
				
			double radius = GeoMath.GetRadius(lat, distanceInMeters);
			double minLat = Math.Max((double)lat - radius, -90.0);
			double maxLat = Math.Min((double)lat + radius,  90.0);
			double minLng = Math.Max((double)lng - radius, -180.0);
			double maxLng = Math.Min((double)lng + radius,  180.0);
			
			long start = (long)Math.Round((minLat + 90.0) * 100000);
			long end = (long)Math.Round((minLng + 180.0) * 100000);
			long minIdx = ZCurve.XY2D(start, end);
			
			start = (long)Math.Round((maxLat + 90.0) * 100000);
			end = (long)Math.Round((maxLng + 180.0) * 100000);
			long maxIdx = ZCurve.XY2D(start, end);
			
  			Location[] candidates = Tree.Find(minIdx, maxIdx);
			Console.WriteLine(" Found " + candidates.Length + " candidates indexed in range "+minIdx+"-"+maxIdx);
			  
			List<Location> locations = new List<Location>();	
      		for (int i = 0; i < candidates.Length; i++) {
        		Location loc = candidates[i];
				bool inRadius = GeoMath.LocateInHaversine(lat, lng, loc.Latitude, loc.Longitude, distanceInMeters);
				if (inRadius) {
					locations.Add(loc);
				}
				
				if (locations.Count == maxResults) {
					break;
				}
			}		
			
			return locations.ToArray();
		}					
	}
}  